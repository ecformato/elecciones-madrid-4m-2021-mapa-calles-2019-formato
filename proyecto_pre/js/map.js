import { text } from 'd3-request';
import { dsvFormat } from 'd3-dsv';

/* VARIABLES DE COLORES (primer color = + claro; segundo color = + oscuro) */
let izquierda_colors = ['#f6f7b3','#e0e400'], 
    derecha_colors = ['#e7e7e7','#000'],
    participacion_colors = ['#E4F4FB','#001A2D'],
    podemos_colors = ['#d7a7dd','#692772'], 
    cs_colors = ['#ffc09f','#f45a09'],
    masmadrid_colors = ['#bae8e4','#20c0b2'],
    pp_colors = ['#c6e5ff','#48aafd'],
    psoe_colors = ['#fbc9c7','#f60b01'],
    vox_colors = ['#d5d9c0','#77be2d'],
    empate = '#e2e2d8',
    sin_datos = '#c6c6c6';

mapboxgl.accessToken = 'pk.eyJ1IjoiZGF0b3MtZWxjb25maWRlbmNpYWwiLCJhIjoiY2syMGI5NHV4MDB1OTNnbnJtZ3UweGh5YiJ9.UrXbe3ehqGCgKdgMgHZnBQ'; //Cambiar

/* Ajustar ZOOM, MINZOOM Y CENTER EN FUNCIÓN DEL DISPOSITIVO */
let mapWidth = document.getElementById('map').clientWidth;
let zoom = mapWidth > 525 ? 8 : 7.5;
let minZoom = mapWidth > 525 ? 7 : 7;
let center = [-3.75, 40.55];
let hoveredStateId = null;

let map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/datos-elconfidencial/ckmowiz7t7pg517qvw94iyz3e',
    attributionControl: false,
    zoom: zoom,
    minZoom: minZoom,
    maxZoom: 16,
    center: center
});

map.addControl(
    new MapboxGeocoder({
        accessToken: mapboxgl.accessToken,
        mapboxgl: mapboxgl,
        language: 'ES',
        marker: false,
        placeholder: 'Busque por calle o municipio'
    })
);

/* Variable para poder navegar sobre el mapa */
let nav = new mapboxgl.NavigationControl({showCompass:false});
map.addControl(nav, 'bottom-left');

/* Inicializar variable para trabajar con popups */
let popup = new mapboxgl.Popup({
    closeButton: false,
    closeOnClick: true
});

map.scrollZoom.disable();

map.on('load', function(){
    let file = 'https://raw.githubusercontent.com/CarlosMunozDiazEC/prueba-datos/main/secciones_madrid_mapbox.csv';

    text(file, function(data){
        let dsv = dsvFormat(";");
        let parseData = dsv.parse(data);

        map.addSource('secciones_censales', {
            'type': 'vector',
            'url': 'mapbox://datos-elconfidencial.bygz4xq7',
            promoteId: 'CUSEC'
        });

        
        parseData.map((item) => {
            map.setFeatureState({
                source: 'secciones_censales',
                sourceLayer: 'secciones_madrid_geo_2019',
                id: +item['Cusec']
            }, {
                //Fuerzas políticas
                pp_porc: +item['% PP'].replace(',','.'),
                psoe_porc: +item['% PSOE'].replace(',','.'),
                masmadrid_porc: +item['% MÁS MADRID'].replace(',','.'),
                podemos_porc: +item['% Podemos'].replace(',','.'),
                vox_porc: +item['% VOX'].replace(',','.'),
                cs_porc: +item['% Cs'].replace(',','.'),

                //Otras variables
                primeraFuerza: item['Primera fuerza'].trim(),
                primeraFuerzaPorc: +item['% PF'].replace(',','.'),
                segundaFuerza: item['Segunda fuerza'].trim(),
                segundaFuerzaPorc: +item['% SF'].replace(',','.'),

                participacion_porc: +item['Participación'].replace(',','.'),                
                bloqueGanador: item['Bloque Gana'].trim(),
                izq_porc: +item['Izquierda'].replace(',','.'),
                der_porc: +item['Derecha'].replace(',','.')
            });
        });

        //Coropletas > Primero rellenamos con ganador
        map.addLayer({
            'id': 'secciones_censales_madrid',
            'source': 'secciones_censales',
            'source-layer': 'secciones_madrid_geo_2019',
            'type': 'fill',
            'paint': {
                'fill-color': [
                    'match',
                    ['feature-state', 'primeraFuerza'],
                    "MÁS MADRID", masmadrid_colors[1],
                    "Cs", cs_colors[1],
                    "PSOE", psoe_colors[1],
                    "PP", pp_colors[1],
                    "PODEMOS - IU", podemos_colors[1],
                    "VOX", vox_colors[1],
                    "Sin datos", sin_datos,
                    empate
                ],
                'fill-opacity': 0.75
            }      
        }, 'road-simple');

        //Límites censales
        map.addLayer({
            'id': 'secciones_censales_line',
            'source': 'secciones_censales',
            'source-layer': 'secciones_madrid_geo_2019',
            'type': 'line',
            'paint': {
                'line-color': [
                    'case',
                    ['boolean', ['feature-state', 'hover'], false],
                    '#000',
                    '#fff'
                ],
                'line-width': [
                    'case',
                    ['boolean', ['feature-state', 'hover'], false],
                    1.75,
                    0.1
                ]
            }      
        }, 'road-label-simple');

        //Eventos con botones
        // document.getElementById('primeraFuerza').addEventListener('click', (e) => {
        //     changeButton(e.target);
        //     changeLegend();
        //     handlePrimera();
        // });

        // document.getElementById('segundaFuerza').addEventListener('click', (e) => {
        //     changeButton(e.target);
        //     changeLegend();
        //     handleSegunda();
        // });

        // document.getElementById('bloqueGanador').addEventListener('click', (e) => {
        //     changeButton(e.target);
        //     changeLegend('legend--bloque-ganador');
        //     handleBloqueGanador();
        // });

        // document.getElementById('porcParticipacion').addEventListener('click', (e) => {
        //     changeButton(e.target);
        //     changeLegend('legend--participacion');
        //     handleParticipacion();
        // });

        // document.getElementById('porcMasMadrid').addEventListener('click', (e) => {
        //     changeButton(e.target);
        //     changeLegend('legend--masmadrid');
        //     handleMasMadrid();
        // });

        // document.getElementById('porcPSOE').addEventListener('click', (e) => {
        //     changeButton(e.target);
        //     changeLegend('legend--psoe');
        //     handlePsoe();
        // });

        // document.getElementById('porcCS').addEventListener('click', (e) => {
        //     changeButton(e.target);
        //     changeLegend('legend--cs');
        //     handleCs();
        // });

        // document.getElementById('porcPP').addEventListener('click', (e) => {
        //     changeButton(e.target);
        //     changeLegend('legend--pp');
        //     handlePp();
        // });

        // document.getElementById('porcPodemos').addEventListener('click', (e) => {
        //     changeButton(e.target);
        //     changeLegend('legend--podemos');
        //     handlePodemos();
        // });

        // document.getElementById('porcVOX').addEventListener('click', (e) => {
        //     changeButton(e.target);
        //     changeLegend('legend--vox');
        //     handleVox();
        // });

        //Eventos de ratón
        bind_event(popup, 'secciones_censales_madrid');
        
        /*
        * Funciones asociadas a eventos en el DOM
        */
        function handlePrimera() {
            map.setPaintProperty('secciones_censales_madrid', 'fill-color', 
                ['match',
                ['feature-state', 'primeraFuerza'],
                "MÁS MADRID", masmadrid_colors[1],
                "Cs", cs_colors[1],
                "PSOE", psoe_colors[1],
                "PP", pp_colors[1],
                "PODEMOS - IU", podemos_colors[1],
                "VOX", vox_colors[1],
                "Sin datos", sin_datos,
                empate
            ]);
        }

        function handleSegunda() {
            map.setPaintProperty('secciones_censales_madrid', 'fill-color', 
                ['match',
                ['feature-state', 'segundaFuerza'],
                "MÁS MADRID", masmadrid_colors[1],
                "Cs", cs_colors[1],
                "PSOE", psoe_colors[1],
                "PP", pp_colors[1],
                "PODEMOS - IU", podemos_colors[1],
                "VOX", vox_colors[1],
                "Sin datos", sin_datos,
                empate
            ]);
        }

        function handleBloqueGanador() {
            map.setPaintProperty('secciones_censales_madrid', 'fill-color', 
                ['match',
                ['feature-state', 'bloqueGanador'],
                "Izquierda", [
                    "interpolate", [
                        "linear", 1
                    ],
                    ['feature-state', 'izq_porc'],
                    45, izquierda_colors[0],
                    100, izquierda_colors[1]
                ],
                "Derecha", [
                    "interpolate", [
                        "linear", 1
                    ],
                    ['feature-state', 'der_porc'],
                    45, derecha_colors[0],
                    100, derecha_colors[1]
                ],
                empate
            ]);
        }

        function handleParticipacion() {
            map.setPaintProperty('secciones_censales_madrid', 'fill-color', 
                ["interpolate", [
                    "linear", 1
                ], ['feature-state', 'participacion_porc'],
                    0, participacion_colors[0],
                    100, participacion_colors[1]
                ]
            );
        }

        function handleMasMadrid() {
            map.setPaintProperty('secciones_censales_madrid', 'fill-color', 
                ["interpolate", [
                    "linear", 1
                ], ['feature-state', 'masmadrid_porc'],
                    0, masmadrid_colors[0],
                    42, masmadrid_colors[1]
                ]
            );
        }

        function handlePsoe() {
            map.setPaintProperty('secciones_censales_madrid', 'fill-color', 
                ["interpolate", [
                    "linear", 1
                ], ['feature-state', 'psoe_porc'],
                    0, psoe_colors[0],
                    57, psoe_colors[1]
                ]
            );
        }

        function handleCs() {
            map.setPaintProperty('secciones_censales_madrid', 'fill-color', 
                ["step",
                ['feature-state', 'cs_porc'],
                    cs_colors[0],
                    10,
                    '#FB9E6D',
                    20,
                    '#F87C3B',
                    30,
                    cs_colors[1]
                ]
                // ["interpolate",
                // ['linear'],
                // ['feature-state', 'cs_porc'],
                //     0, cs_colors[0],
                //     48, cs_colors[1]
                // ]
            );
        }

        function handlePp() {
            map.setPaintProperty('secciones_censales_madrid', 'fill-color', 
                ["interpolate", [
                    "linear", 1
                ], ['feature-state', 'pp_porc'],
                    0, pp_colors[0],
                    65, pp_colors[1]
                ]
            );
        }

        function handlePodemos() {
            map.setPaintProperty('secciones_censales_madrid', 'fill-color', 
                ["interpolate", [
                    "linear", 1
                ], ['feature-state', 'podemos_porc'],
                    0, podemos_colors[0],
                    20, podemos_colors[1]
                ]
            );
        }

        function handleVox() {
            map.setPaintProperty('secciones_censales_madrid', 'fill-color', 
                ["interpolate", [
                    "linear", 1
                ], ['feature-state', 'vox_porc'],
                    0, vox_colors[0],
                    36, vox_colors[1]
                ]
            );
        }
    });
});

////// Eventos DOM > Efecto en Mapbox //////
//Selección de botones
let currentButton = document.querySelector('button.selected');
function changeButton(button){
    currentButton.classList.remove('selected');
    button.classList.add('selected');
    currentButton = button;
}

//Cambio leyendas
let legends = document.getElementsByClassName('legends')[0];
let currentLegend = undefined;
function changeLegend(legend = undefined){
    if(currentLegend){
        currentLegend.classList.remove('selected');
    }
    
    if(!legend){  
        currentLegend = legend;
    } else {
        let aux = legends.getElementsByClassName(`${legend}`)[0];
        aux.classList.add('selected');
        currentLegend = aux;
    }   
}


////// Otros elementos del DOM > Eventos responsive + Tooltip ////////
// if(window.innerWidth < 640){
//     map.dragPan.disable();
//     map.on('zoom', function(){
//         let zoom = map.getZoom();
        
//         if(zoom > 7){
//             map.dragPan.enable();
//         } else {
//             map.dragPan.disable();
//         }
//     });
// }

// //Cambio texto en dos botones en mobile
// if(window.innerWidth < 525){
//     document.getElementById('primeraFuerza').textContent = '1ª fuerza';
//     document.getElementById('segundaFuerza').textContent = '2ª fuerza';
//     document.getElementById('ganadorBloque').textContent = 'Izq vs dcha';
//     document.getElementById('participacion').textContent = 'Particip.';
// }

//Uso del tooltip
function bind_event(popup, id){
    map.on('mousemove', id, function(e){
        //Primera parte
        let propiedades = e.features[0];
        map.getCanvas().style.cursor = 'pointer';
        var coordinates = e.lngLat;       
        var tooltipText = get_tooltip_text(propiedades);
        while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
            coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
        }        
        popup.setLngLat(coordinates)
            .setHTML(tooltipText)
            .addTo(map);

        //Segunda parte
        if (e.features.length > 0) {
            if (hoveredStateId) {
            map.setFeatureState({
                source: 'secciones_censales',
                sourceLayer: 'secciones_madrid_geo_2019',
                id: hoveredStateId
            }, {
                hover: false
            });
            }
            hoveredStateId = e.features[0].id;
            map.setFeatureState({
                source: 'secciones_censales',
                sourceLayer: 'secciones_madrid_geo_2019',
                id: hoveredStateId
            }, {
                hover: true
            });
        }
        map.getCanvas().style.cursor = 'pointer';
    });
    map.on('mouseleave', id, function() {
        map.getCanvas().style.cursor = '';
        popup.remove();

        if (hoveredStateId) {
        map.setFeatureState({
            source: 'secciones_censales',
            sourceLayer: 'secciones_madrid_geo_2019',
            id: hoveredStateId
        }, {
            hover: false
        });
        }
        hoveredStateId = null;
    });
}  

function get_tooltip_text(propiedades){
    let caracteristicas_fijas = propiedades.properties;
    let estado = propiedades.state;
    
    let html = '';
    html = `<div class="widget__title">${caracteristicas_fijas.NMUN}</b> (Part.: ${estado.participacion_porc ? estado.participacion_porc.toFixed(2) : 'Sin datos'}%)</div>
            <p>Primera fuerza: ${estado.primeraFuerza}</p>
            <p>Segunda: ${estado.segundaFuerza}</p>
            <p style="margin-bottom: 3px"><span style="border-bottom: 1px solid #e0e400">Izq.</span> vs. <span style="border-bottom: 1px solid #000">Dcha.</span></p>
            <div style="display: flex; padding-bottom: 3px; border-bottom: 1px solid #ccc">
                <div style="height: 10px; background: #e0e400; width: ${estado.izq_porc}%;"></div>
                <div style="height: 10px; background: #000; width: ${estado.der_porc}%;"></div>
            </div>
            <span style="display: block; height: 16.5px;">MM: ${estado.masmadrid_porc}%</span>
            <span style="display: block; height: 16.5px;">Podemos: ${estado.podemos_porc}%</span>
            <span style="display: block; height: 16.5px;">PSOE: ${estado.psoe_porc}%</span>
            <span style="display: block; height: 16.5px;">Cs: ${estado.cs_porc}%</span>            
            <span style="display: block; height: 16.5px;">PP: ${estado.pp_porc}%</span>
            <span style="display: block; height: 16.5px;">VOX: ${estado.vox_porc}%</span>`;    
    return html; 
}